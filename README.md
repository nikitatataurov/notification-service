# notification-service

## Name
Notification service

## Done 1, 3, 5, 9 additional tasks 
## Description
Service for creating mailing, clients and making auto-mailings with support of notification service. Look up OpenApi specification below

## Installation
Steps to deploy:
- git clone project;
- cd notification_service;
- prepare notification_service/backend/.env.dev file (look up example below)
- docker-compose up --build;
- look up /docs for SwaggerUI.

### .env.dev template
DEBUG=1
SECRET_KEY=your_secret_key
DJANGO_ALLOWED_HOSTS=localhost 127.0.0.1 0.0.0.0
DJANGO_CORS_ALLOWED_ORIGINS=http://localhost:8080 http://172.18.0.4:8080 http://192.168.0.16:8080
SQL_ENGINE=django.db.backends.postgresql
SQL_DATABASE=notification_db
SQL_USER=user
SQL_PASSWORD=password
SQL_HOST=db
SQL_PORT=5432
SMS_API_TOKEN=jwt_token
SMS_API_URL=sms_api_url

## Usage
SwaggerUI show all need urls:
- Create new client `/clients` 
- Update client `/clients/:id`
- Delete client `/clients/:id`
- Create mailing `/mailings`
- Get general stat by mailings grouped by statuses `/mailings/general_stat`
- Get detail messages stat by mailing `/mailings/:id/stat`
- Update mailing `/mailings/:id`
- Delete mailing `/mailings/:id`
- On create or update mailing celery-task created and pushed to broker, then checks and with MailingService sending to API
- With celery-beat organised every n-seconds checking available mailing and clients which need to receive message (messages for every client and mailing are unique and message status checked before sending for client, only new and fail statuses will send)  
- For create time-interval send request to `/mailings/set_sending_period?seconds=n` where n is integer seconds amount

## Authors and acknowledgment
tataurovns@mail.ru(Nikita Tataurov)

## OpenApi specification
```json
{
  "openapi": "3.0.2",
  "info": {
    "title": "Notification service",
    "version": "1.0.0",
    "description": "Notification service for mailing clients"
  },
  "paths": {
    "/api/v1/clients/": {
      "get": {
        "operationId": "listClients",
        "description": "ViewSet for work with clients",
        "parameters": [],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Client"
                  }
                }
              }
            },
            "description": ""
          }
        },
        "tags": [
          "api"
        ]
      },
      "post": {
        "operationId": "createClient",
        "description": "ViewSet for work with clients",
        "parameters": [],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Client"
              }
            },
            "application/x-www-form-urlencoded": {
              "schema": {
                "$ref": "#/components/schemas/Client"
              }
            },
            "multipart/form-data": {
              "schema": {
                "$ref": "#/components/schemas/Client"
              }
            }
          }
        },
        "responses": {
          "201": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Client"
                }
              }
            },
            "description": ""
          }
        },
        "tags": [
          "api"
        ]
      }
    },
    "/api/v1/clients/{id}/": {
      "get": {
        "operationId": "retrieveClient",
        "description": "ViewSet for work with clients",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "description": "A unique integer value identifying this client.",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Client"
                }
              }
            },
            "description": ""
          }
        },
        "tags": [
          "api"
        ]
      },
      "put": {
        "operationId": "updateClient",
        "description": "ViewSet for work with clients",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "description": "A unique integer value identifying this client.",
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Client"
              }
            },
            "application/x-www-form-urlencoded": {
              "schema": {
                "$ref": "#/components/schemas/Client"
              }
            },
            "multipart/form-data": {
              "schema": {
                "$ref": "#/components/schemas/Client"
              }
            }
          }
        },
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Client"
                }
              }
            },
            "description": ""
          }
        },
        "tags": [
          "api"
        ]
      },
      "patch": {
        "operationId": "partialUpdateClient",
        "description": "ViewSet for work with clients",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "description": "A unique integer value identifying this client.",
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Client"
              }
            },
            "application/x-www-form-urlencoded": {
              "schema": {
                "$ref": "#/components/schemas/Client"
              }
            },
            "multipart/form-data": {
              "schema": {
                "$ref": "#/components/schemas/Client"
              }
            }
          }
        },
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Client"
                }
              }
            },
            "description": ""
          }
        },
        "tags": [
          "api"
        ]
      },
      "delete": {
        "operationId": "destroyClient",
        "description": "ViewSet for work with clients",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "description": "A unique integer value identifying this client.",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "204": {
            "description": ""
          }
        },
        "tags": [
          "api"
        ]
      }
    },
    "/api/v1/mailings/": {
      "get": {
        "operationId": "listMailings",
        "description": "ViewSet for work with mailings",
        "parameters": [],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "type": "array",
                  "items": {
                    "$ref": "#/components/schemas/Mailing"
                  }
                }
              }
            },
            "description": ""
          }
        },
        "tags": [
          "api"
        ]
      },
      "post": {
        "operationId": "createMailing",
        "description": "ViewSet for work with mailings",
        "parameters": [],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Mailing"
              }
            },
            "application/x-www-form-urlencoded": {
              "schema": {
                "$ref": "#/components/schemas/Mailing"
              }
            },
            "multipart/form-data": {
              "schema": {
                "$ref": "#/components/schemas/Mailing"
              }
            }
          }
        },
        "responses": {
          "201": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Mailing"
                }
              }
            },
            "description": ""
          }
        },
        "tags": [
          "api"
        ]
      }
    },
    "/api/v1/mailings/general_stat/": {
      "get": {
        "operationId": "generalStatMailing",
        "description": "Show general stat of mailings",
        "parameters": [],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Mailing"
                }
              }
            },
            "description": ""
          }
        },
        "tags": [
          "api"
        ]
      }
    },
    "/api/v1/mailings/set_sending_period/": {
      "get": {
        "operationId": "setMailingPeriodMailing",
        "description": "ViewSet for work with mailings",
        "parameters": [],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Mailing"
                }
              }
            },
            "description": ""
          }
        },
        "tags": [
          "api"
        ]
      }
    },
    "/api/v1/mailings/{id}/": {
      "get": {
        "operationId": "retrieveMailing",
        "description": "ViewSet for work with mailings",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "description": "A unique integer value identifying this mailing.",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Mailing"
                }
              }
            },
            "description": ""
          }
        },
        "tags": [
          "api"
        ]
      },
      "put": {
        "operationId": "updateMailing",
        "description": "ViewSet for work with mailings",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "description": "A unique integer value identifying this mailing.",
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Mailing"
              }
            },
            "application/x-www-form-urlencoded": {
              "schema": {
                "$ref": "#/components/schemas/Mailing"
              }
            },
            "multipart/form-data": {
              "schema": {
                "$ref": "#/components/schemas/Mailing"
              }
            }
          }
        },
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Mailing"
                }
              }
            },
            "description": ""
          }
        },
        "tags": [
          "api"
        ]
      },
      "patch": {
        "operationId": "partialUpdateMailing",
        "description": "ViewSet for work with mailings",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "description": "A unique integer value identifying this mailing.",
            "schema": {
              "type": "string"
            }
          }
        ],
        "requestBody": {
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/Mailing"
              }
            },
            "application/x-www-form-urlencoded": {
              "schema": {
                "$ref": "#/components/schemas/Mailing"
              }
            },
            "multipart/form-data": {
              "schema": {
                "$ref": "#/components/schemas/Mailing"
              }
            }
          }
        },
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Mailing"
                }
              }
            },
            "description": ""
          }
        },
        "tags": [
          "api"
        ]
      },
      "delete": {
        "operationId": "destroyMailing",
        "description": "ViewSet for work with mailings",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "description": "A unique integer value identifying this mailing.",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "204": {
            "description": ""
          }
        },
        "tags": [
          "api"
        ]
      }
    },
    "/api/v1/mailings/{id}/stat/": {
      "get": {
        "operationId": "statMailing",
        "description": "Show stat for specific instance",
        "parameters": [
          {
            "name": "id",
            "in": "path",
            "required": true,
            "description": "A unique integer value identifying this mailing.",
            "schema": {
              "type": "string"
            }
          }
        ],
        "responses": {
          "200": {
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/Mailing"
                }
              }
            },
            "description": ""
          }
        },
        "tags": [
          "api"
        ]
      }
    }
  },
  "components": {
    "schemas": {
      "Client": {
        "type": "object",
        "properties": {
          "phone_number": {
            "type": "string",
            "pattern": "7\\d{10}",
            "maxLength": 11
          },
          "phone_code": {
            "type": "integer",
            "maximum": 999,
            "minimum": 100
          },
          "tag": {
            "type": "string",
            "maxLength": 55
          },
          "time_zone_code": {
            "type": "string",
            "maxLength": 25
          }
        },
        "required": [
          "phone_number",
          "phone_code",
          "tag",
          "time_zone_code"
        ]
      },
      "Mailing": {
        "type": "object",
        "properties": {
          "id": {
            "type": "integer",
            "readOnly": true
          },
          "content": {
            "type": "string"
          },
          "start_at": {
            "type": "string",
            "format": "date-time"
          },
          "end_at": {
            "type": "string",
            "format": "date-time"
          },
          "client_filter": {
            "type": "object",
            "properties": {
              "id": {
                "type": "integer",
                "readOnly": true
              },
              "type": {
                "enum": [
                  "phone_number",
                  "phone_code",
                  "tag"
                ],
                "type": "string"
              },
              "value": {
                "type": "string",
                "maxLength": 55
              }
            },
            "required": [
              "value"
            ]
          }
        },
        "required": [
          "content",
          "start_at",
          "end_at",
          "client_filter"
        ]
      }
    }
  }
}
```
