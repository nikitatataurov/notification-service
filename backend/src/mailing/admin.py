from django.contrib import admin

from .models import Client, ClientFilter, Mailing, Message

admin.site.register(Client)
admin.site.register(ClientFilter)
admin.site.register(Mailing)
admin.site.register(Message)
