from django.core import validators
from django.db import models


class Client(models.Model):
    """Model for client entity"""
    phone_number = models.CharField(
        validators=[
            validators.RegexValidator(
                regex=r'7\d{10}',
                message="Phone number must be entered as 7XXXXXXXXXX (X - digit from 0 to 9)"
            ),
        ],
        blank=False,
        null=False,
        max_length=11,
    )
    phone_code = models.IntegerField(
        validators=[
            validators.MinValueValidator(100),
            validators.MaxValueValidator(999),
        ],
        blank=False,
        null=False,
    )
    tag = models.CharField(max_length=55, blank=False, null=False)
    time_zone_code = models.CharField(
        blank=False,
        null=False,
        max_length=25,
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.phone_number
