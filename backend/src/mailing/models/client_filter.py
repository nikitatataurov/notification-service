from django.db import models

from ..value_objects import ClientFilterType


class ClientFilter(models.Model):
    """Model for client filter pairs"""
    type = models.CharField(max_length=12, choices=ClientFilterType.choices, default=ClientFilterType.PHONE_NUMBER)
    value = models.CharField(max_length=55, null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        """Not needed to duplicate client filters"""
        constraints = [
            models.UniqueConstraint(fields=['type', 'value'], name='unique_value_type_for_client_filter'),
        ]

    def __str__(self):
        return f'{self.type} - {self.value}'
