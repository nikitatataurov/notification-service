from django.db import models

from .client_filter import ClientFilter


class Mailing(models.Model):
    """Model for mailing entity"""
    content = models.TextField(null=False, blank=False)
    start_at = models.DateTimeField(null=False, blank=False)
    end_at = models.DateTimeField(null=False, blank=False)
    client_filter = models.ForeignKey(
        ClientFilter,
        related_name='mailings',
        on_delete=models.CASCADE,
        blank=False,
        null=False,
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.content[:25]
