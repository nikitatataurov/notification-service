from django.db import models

from . import Mailing, Client
from ..value_objects import MessageStatus


class Message(models.Model):
    """Model for message entity"""
    status = models.CharField(max_length=7, choices=MessageStatus.choices, default=MessageStatus.NEW)
    mailing = models.ForeignKey(Mailing, related_name='messages', on_delete=models.CASCADE, blank=False, null=False)
    client = models.ForeignKey(Client, related_name='messages', on_delete=models.CASCADE, blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        """For every client max one message for one mailing per client"""
        constraints = [
            models.UniqueConstraint(fields=['client', 'mailing'], name='unique_client_mailing_for_message'),
        ]

    def __str__(self):
        return f'{self.client.phone_number} - "{self.mailing}"'
