from ..models import ClientFilter
from ..value_objects import ClientFilterType


class ClientFilterRepository:
    """Repository for work data related to client_filter"""

    def get_or_create_by_type_and_value(self, filter_type: ClientFilterType, filter_value: str) -> ClientFilter:
        """Get or create client_filter by type and value"""
        return ClientFilter.objects.get_or_create(type=filter_type, value=filter_value)[0]
