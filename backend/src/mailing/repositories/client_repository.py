from django.db.models import QuerySet

from ..models import Client, ClientFilter


class ClientRepository:
    """Repository for fetching data related to client"""

    def get_by_filter(self, client_filter: ClientFilter) -> QuerySet[Client]:
        """Get all clients by client filter"""
        return Client.objects.filter(**{client_filter.type: client_filter.value})
