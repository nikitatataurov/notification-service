import datetime

from django.db.models import QuerySet

from ..models import Mailing


class MailingRepository:
    """Repository for fetching data related to mailing"""

    def get_by_datetime(self, filter_date_time: datetime.datetime) -> QuerySet[Mailing]:
        """Get all mailings by filter date time"""
        return Mailing.objects.filter(start_at__lt=filter_date_time, end_at__gt=filter_date_time)

    def get_by_pk(self, pk: int) -> Mailing:
        """Get mailing by id"""
        return Mailing.objects.get(pk=pk)
