from django.db.models import QuerySet

from ..models import Client, Mailing, Message


class MessageRepository:
    """Repository for fetching data related to messages"""

    def get_by_mailing(self, mailing: int) -> QuerySet[Message]:
        """Get all messages by mailing"""
        return Message.objects.filter(mailing=mailing)

    def get_by_status(self, status: str) -> QuerySet[Message]:
        """Get all messages by status"""
        return Message.objects.filter(status=status)

    def get_or_create_by_client_and_mailing(self, client: Client, mailing: Mailing) -> Message:
        """Get or create message by client and mailing"""
        return Message.objects.get_or_create(client=client, mailing=mailing)[0]
