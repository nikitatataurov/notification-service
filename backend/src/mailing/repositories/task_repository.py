from django_celery_beat.models import PeriodicTask, IntervalSchedule


class TaskRepository:
    """Repository for fetching data related to tasks"""
    def get_by_task_type(self, task_type: str) -> None | PeriodicTask:
        """Get by task type"""
        return PeriodicTask.objects.filter(task=task_type).first()

    def get_or_create_interval(self, every: int, period: str) -> IntervalSchedule:
        """Get or create interval"""
        return IntervalSchedule.objects.get_or_create(every=every, period=period)[0]
