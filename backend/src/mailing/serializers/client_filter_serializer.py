from rest_framework import serializers

from ..models import ClientFilter


class ClientFilterSerializer(serializers.ModelSerializer):
    """Serializer for ClientFilter model"""
    class Meta:
        model = ClientFilter
        read_only_fields = (
            'id',
        )
        fields = (
            'id',
            'type',
            'value',
        )
