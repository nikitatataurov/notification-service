from rest_framework import serializers

from ..models import Client


class ClientSerializer(serializers.ModelSerializer):
    """Serializer for client model"""

    class Meta:
        model = Client
        read_only_fields = (
            'id',
        )
        fields = (
            'phone_number',
            'phone_code',
            'tag',
            'time_zone_code',
        )
