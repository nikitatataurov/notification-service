from rest_framework import serializers

from .client_filter_serializer import ClientFilterSerializer
from .. import tasks
from ..models import ClientFilter, Mailing
from ..repositories import ClientFilterRepository


class MailingSerializer(serializers.ModelSerializer):
    client_filter = ClientFilterSerializer(many=False)
    """Serializer for mailing model"""
    class Meta:
        model = Mailing
        read_only_fields = (
            'id',
            'created_at',
        )
        fields = (
            'id',
            'content',
            'start_at',
            'end_at',
            'client_filter',
        )

    def create(self, validated_data):
        """Saving mailing and client_filter models and push `send_message` task"""
        client_filter_data: dict = validated_data.pop('client_filter')
        client_filter: ClientFilter = ClientFilterRepository().get_or_create_by_type_and_value(
            filter_type=client_filter_data['type'],
            filter_value=client_filter_data['value'],
        )

        mailing: Mailing = Mailing.objects.create(**validated_data, client_filter=client_filter)
        tasks.send_message.delay(mailing_pk=mailing.pk)

        return mailing

    def update(self, instance, validated_data):
        """Updating mailing and client_filter models and push `send_message` task"""
        client_filter: dict = validated_data.pop('client_filter')

        for attr, value in validated_data.items():
            setattr(instance, attr, value)
        instance.save()
        tasks.send_message.delay(mailing_pk=instance.pk)

        client_filter_instance: ClientFilter = instance.client_filter
        for attr, value in client_filter.items():
            setattr(client_filter_instance, attr, value)
        client_filter_instance.save()

        return instance

