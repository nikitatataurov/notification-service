from rest_framework import serializers

from .client_serializer import ClientSerializer
from ..models import Message


class MessageSerializer(serializers.ModelSerializer):
    """Serializer for message model"""
    client = ClientSerializer(many=False)

    class Meta:
        model = Message
        read_only_fields = (
            'created_at',
            'updated_at',
        )
        fields = (
            'status',
            'client',
        )
