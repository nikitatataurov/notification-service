import os

import requests

from ..models import Mailing, Message
from ..repositories import ClientRepository, MessageRepository
from ..value_objects import MessageStatus


class MailingService:
    """Service for sending messages"""
    def send(self, mailing: Mailing) -> None:
        """Sending messages to api"""
        client_repository: ClientRepository = ClientRepository()
        message_repository: MessageRepository = MessageRepository()
        for client in client_repository.get_by_filter(client_filter=mailing.client_filter):
            message: Message = message_repository.get_or_create_by_client_and_mailing(client=client, mailing=mailing)

            if message.status == MessageStatus.SUCCESS:
                continue

            status: str = MessageStatus.FAIL
            try:
                message_id: int = message.pk
                response = requests.post(
                    f'{os.getenv("SMS_API_URL")}/v1/send/{message_id}',
                    json={'id': message_id, 'phone': int(client.phone_number), 'text': mailing.content},
                    headers={
                        'accept': 'application/json',
                        'Authorization': f'Bearer {os.getenv("SMS_API_TOKEN")}',
                        'Content-Type': 'application/json',
                    }
                )
                if 200 == response.status_code:
                    status = MessageStatus.SUCCESS
            except:
                pass
            message.status = status
            message.save()
