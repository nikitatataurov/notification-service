import datetime as dt

import pytz
from celery import shared_task

from .repositories import MailingRepository
from .services import MailingService


@shared_task
def send_messages():
    """Task for taking current needed mailings and prepare send message task"""
    for mailing in MailingRepository().get_by_datetime(filter_date_time=dt.datetime.now(tz=pytz.UTC)):
        send_message.delay(mailing_pk=mailing.pk)


@shared_task
def send_message(mailing_pk: int):
    """Task for sending messages by mailing"""
    now = dt.datetime.now(tz=pytz.UTC)
    mailing = MailingRepository().get_by_pk(mailing_pk)
    if mailing.start_at < now < mailing.end_at:
        MailingService().send(mailing=mailing)
