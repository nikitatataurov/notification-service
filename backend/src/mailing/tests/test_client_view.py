from rest_framework import status
from rest_framework.test import APIClient, APITestCase
from django import urls
from ..models import Client


class ClientViewTestCase(APITestCase):
    """Test ClientViewSet"""
    base_url = urls.reverse('mailing:clients-list')

    def setUp(self):
        self.api_client = APIClient()

        Client.objects.create(
            phone_number='79181111111',
            phone_code=918,
            tag='test1',
            time_zone_code='Europe/Moscow',
        )
        Client.objects.create(
            phone_number='79181233413',
            phone_code=918,
            tag='test2',
            time_zone_code='Europe/Moscow',
        )

    def test_client_list(self):
        """Test get clients"""
        response = self.api_client.get(self.base_url)
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 2

    def test_client_create(self):
        """Test post client"""
        response = self.api_client.post(
            self.base_url,
            {
                'phone_number': '79181111111',
                'phone_code': 918,
                'tag': 'test3',
                'time_zone_code': 'Europe/Moscow',
            },
            format='json',
        )
        assert response.status_code == status.HTTP_201_CREATED
        assert Client.objects.all().count() == 3

# todo continued...
