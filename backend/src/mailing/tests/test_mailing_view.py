import datetime

import pytz
from rest_framework import status
from rest_framework.test import APIClient, APITestCase
from django import urls
from ..models import ClientFilter, Mailing
from ..value_objects import ClientFilterType


class MailingViewTest(APITestCase):
    """Test MailingViewSet"""
    base_url = urls.reverse('mailing:mailings-list')

    def setUp(self):
        self.api_client = APIClient()

        client_filter: ClientFilter = ClientFilter.objects.create(
            type=ClientFilterType.PHONE_NUMBER,
            value='79181231313'
        )
        utc = pytz.UTC
        Mailing.objects.create(
            content='test1',
            start_at=datetime.datetime.now(tz=utc),
            end_at=(datetime.datetime.now(tz=utc) + datetime.timedelta(days=1)),
            client_filter=client_filter,
        )
        Mailing.objects.create(
            content='test2',
            start_at=datetime.datetime.now(tz=utc),
            end_at=(datetime.datetime.now(tz=utc) + datetime.timedelta(days=3)),
            client_filter=client_filter,
        )

    def test_mailing_list(self):
        """Test get mailings"""
        response = self.api_client.get(self.base_url)
        assert response.status_code == status.HTTP_200_OK
        assert len(response.data) == 2

    def test_client_create(self):
        """Test post mailing"""
        response = self.api_client.post(
            self.base_url,
            {
                "content": "Test3",
                "start_at": "2022-12-21T22:31:00+03:00",
                "end_at": "2022-12-21T12:32:00+03:00",
                "client_filter": {
                    "type": "tag",
                    "value": "test3_tag"
                }
            },
            format='json',
        )
        assert response.status_code == status.HTTP_201_CREATED
        assert Mailing.objects.all().count() == 3

# todo continued...
