from rest_framework.routers import DefaultRouter

from .views import ClientViewSet, MailingViewSet

app_name = 'mailing'

router = DefaultRouter()
router.register('clients', ClientViewSet, basename='clients')
router.register('mailings', MailingViewSet, basename='mailings')

urlpatterns = router.urls
