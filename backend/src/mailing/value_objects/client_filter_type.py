from django.utils.translation import gettext_lazy
from django.db import models


class ClientFilterType(models.TextChoices):
    """Value object for client filter types"""
    PHONE_NUMBER = 'phone_number', gettext_lazy('Phone number')
    PHONE_CODE = 'phone_code', gettext_lazy('Phone code')
    TAG = 'tag', gettext_lazy('Tag')
