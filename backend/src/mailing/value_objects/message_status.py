from django.utils.translation import gettext_lazy
from django.db import models


class MessageStatus(models.TextChoices):
    """Value object for message statuses"""
    NEW = 'new', gettext_lazy('New')
    SUCCESS = 'success', gettext_lazy('Success')
    FAIL = 'fail', gettext_lazy('Fail')
