from rest_framework import viewsets

from ..models import Client
from ..serializers import ClientSerializer


class ClientViewSet(viewsets.ModelViewSet):
    """ViewSet for work with clients"""
    serializer_class = ClientSerializer
    queryset = Client.objects.all()
