from django.db.models import QuerySet
from django_celery_beat.models import PeriodicTask, IntervalSchedule
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from ..models import Mailing, Message
from ..repositories import MessageRepository
from ..repositories.task_repository import TaskRepository
from ..serializers import MessageSerializer, MailingSerializer
from ..value_objects import MessageStatus


class MailingViewSet(viewsets.ModelViewSet):
    """ViewSet for work with mailings"""
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()

    @action(detail=False)
    def general_stat(self, request):
        """Show general stat of mailings"""
        message_repository: MessageRepository = MessageRepository()
        response_data: dict = {}
        for status in MessageStatus:
            messages: QuerySet[Message] = message_repository.get_by_status(status=status)
            response_data[status] = {
                'count': messages.count(),
                'mailings': [MailingSerializer(message.mailing).data for message in messages],
            }

        return Response(response_data)

    @action(detail=True)
    def stat(self, request, pk):
        """Show stat for specific instance"""
        return Response(MessageSerializer(MessageRepository().get_by_mailing(mailing=pk), many=True).data)

    @action(detail=False)
    def set_sending_period(self, request):
        seconds: str = request.GET.get('seconds')
        if not seconds or not seconds.isdigit():
            return Response({'code': 400, 'message': 'Incorrect GET params'})

        task_repository = TaskRepository()
        schedule: IntervalSchedule = task_repository.get_or_create_interval(every=int(seconds), period=IntervalSchedule.SECONDS)
        task_type: str = 'src.mailing.tasks.send_messages'
        periodic_task: PeriodicTask | None = task_repository.get_by_task_type(task_type=task_type)
        if periodic_task:
            periodic_task.interval = schedule
            periodic_task.save()
        else:
            PeriodicTask.objects.create(
                interval=schedule,
                name='Mailing',
                task=task_type,
            )
        
        return Response({'code': 201, 'message': 'OK'})
